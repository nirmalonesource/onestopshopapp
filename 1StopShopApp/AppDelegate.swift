//
//  AppDelegate.swift
//  1StopShopApp
//
//  Created by My Mac on 22/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?
    let db = SQLiteDB.shared
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      //  _ = db.open(copyFile:true)
         IQKeyboardManager.shared.enable = true
        
        if UserDefaults.standard.object(forKey: "items") != nil {
                   GlobalVariables.globalarray = UserDefaults.standard.array(forKey: "items") as! [[String : Any]]
                 //  productlist = GlobalVariables.globalarray
                 //  productlist = GlobalVariables.globalarray
                      }
        Autologin()
        SetupOneSignal(launchOptions: launchOptions)
        return true
    }
    
    func SetupOneSignal(launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
    {
        //Remove this method to stop OneSignal Debugging
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)

        //START OneSignal initialization code
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
        
        // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
          appId: "7db83eb0-7103-49a6-8fe0-45ac2b113be8",
          handleNotificationAction: nil,
          settings: onesignalInitSettings)

        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

        // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
          print("User accepted notifications: \(accepted)")
        })
        
        let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
        StoredData.shared.playerId = userId
        //END OneSignal initializataion code
    }
    
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        // prints out all properties
        print("PermissionStateChanges: \n\(String(describing: stateChanges))")
    }
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        
        print("SubscriptionStateChange: \n\(String(describing: stateChanges))")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            print("Current playerId \(playerId)")
            StoredData.shared.playerId = playerId
           // UserDefaults.standard.set(playerId, forKey: "playerId")
            //kUserDefault.synchronize()
        }
    }
    
    func Autologin()
    {
        if UserDefaults.standard.getIsLogin() {
            
            guard let rootVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController else {
                           return
                       }
                       let navigationController = UINavigationController(rootViewController: rootVC)
            navigationController.navigationBar.isHidden = true
                       UIApplication.shared.windows.first?.rootViewController = navigationController
                       UIApplication.shared.windows.first?.makeKeyAndVisible()
            
             }
    }

    // MARK: UISceneSession Lifecycle

//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }


}

