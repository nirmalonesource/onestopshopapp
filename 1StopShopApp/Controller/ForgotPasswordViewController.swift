//
//  ForgotPasswordViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 23/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet var lbldescr: UILabel!
    @IBOutlet var txtemail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func validation()
                 {
                     self.view.endEditing(true)
                         
                      if !txtemail.isValidEmail()
                     {
                      showToast(uiview: self, msg: "Please enter EmailId")
                     }
                     else
                     {
                         ForgotPasswordApi()
                     }
                 }
       
         //MARK: - Web Services
          
           
            func ForgotPasswordApi()
                  {
                      if !isInternetAvailable(){
                          noInternetConnectionAlert(uiview: self)
                      }
                      else
                      {
                       let parameters = ["email" : txtemail.text!,
                              ] as [String : Any]
                        let url = ServiceList.SERVICE_URL+ServiceList.FORGOT_PASSWORD_API
        
                           callApi(url,
                                         method: .post,
                                         param: parameters, withLoader: true)
                                 { (result) in
                                      print("LOGINRESPONSE:",result)
                                      if result.getBool(key: "status")
                                      {
                                       
                                       let alertController = UIAlertController(title: "", message: result.getString(key: "message"), preferredStyle:UIAlertController.Style.alert)
                                       
                                       if let popoverController = alertController.popoverPresentationController {
                                                  popoverController.sourceView = self.view //to set the source of your alert
                                                  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                                                            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                                                        }

                                       alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                                         { action -> Void in
                                           // Put your code here
                                           self.navigationController?.popViewController(animated: true)

                                         })
                                       self.present(alertController, animated: true, completion: nil)
                                      }
                                   else
                                   {
                                       showToast(uiview: self, msg: result.getString(key: "message"))
                                   }
                          }
                      }
                      
                  }
       
    
    // MARK: - Button Action

     @IBAction func submit_click(_ sender: UIButton) {
        
        validation()
     }
    
     
     @IBAction func account_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
     }
    

}
