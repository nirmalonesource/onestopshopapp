//
//  OrderDetailViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 11/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class orderdetailcell: UITableViewCell {
    @IBOutlet var lblproductname: UILabel!
    @IBOutlet var lblproductdetail: UILabel!
    
}

class OrderDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var lblorderno: UILabel!
      @IBOutlet var lbldate: UILabel!
      @IBOutlet var lblstatus: UILabel!
      @IBOutlet var lblamt: UILabel!
    
    @IBOutlet var tblorderdetail: UITableView!
      
    var OrderDetailList = [[String:Any]]()
    var OrderDict = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
     //   let dict:[String:Any] = OrderList[indexPath.row]
        lblorderno.text = OrderDict["OrderNumber"] as? String
        lblstatus.text = OrderDict["Order_Status"] as? String
        lblamt.text = "Rs " + String(OrderDict["Order_Item_Price"] as? String ?? "0")
        lbldate.text = convertDateString(dateString: OrderDict["CreatedOn"] as? String, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yyyy")
        
        if  OrderDict["Order_Status"] as? String == "complete" {
            lblstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
        }
        else if OrderDict["Order_Status"] as? String == "pending"
        {
            lblstatus.textColor = UIColor.systemOrange
        }
        else
        {
            lblstatus.textColor = UIColor.systemRed
        }
        
        OrderDetailList = OrderDict["Order_details"] as? [[String : Any]] ?? []
    }
    

      //MARK: - Tableview Data Source
                  
                  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                   
                     return OrderDetailList.count
                     
                  }
                  
                  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                   
                      let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailcell", for: indexPath) as! orderdetailcell
                     let dict:[String:Any] = OrderDetailList[indexPath.row]
                     cell.lblproductname.text = dict["ProductName"] as? String
                    // cell.lblproductdetail.text = String(dict["UOMQty"] as? String ?? "0") + String(dict["UOMDesc"] as? String ?? "0") + "-" + "Rs " + String(dict["TotalPrice"] as? String ?? "0")
                    
                    let Product_detail = dict["Product_detail"] as? [[String:Any]]
                    if Product_detail?.count ?? 0 > 0 {
                        cell.lblproductdetail.text = "\(dict["UOMQty"] as? String ?? "0") \(dict["UOMDesc"] as? String ?? "0") - Rs \(dict["TotalPrice"] as? String ?? "0")"
                    }
                    else
                    {
                        cell.lblproductdetail.text = "Rs \(dict["UnitPrice"] as? String ?? "0") * \(dict["Qty"] as? String ?? "0") QTY"
                    }
                    
//                     cell.lblamt.text = "Rs " + String(dict["Order_Item_Price"] as? String ?? "0")
//                     cell.lbldate.text = convertDateString(dateString: dict["CreatedOn"] as? String, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yyyy")
//
//                     if  dict["Order_Status"] as? String == "complete" {
//                         cell.lblstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
//                     }
//                     else if dict["Order_Status"] as? String == "pending"
//                     {
//                         cell.lblstatus.textColor = UIColor.systemOrange
//                     }
//                     else
//                     {
//                         cell.lblstatus.textColor = UIColor.systemRed
//                     }
                      return cell
                  }
             
             
             
             func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                 
    
             }
     
     // MARK: - Button Action
        
        @IBAction func back_click(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
    

}
