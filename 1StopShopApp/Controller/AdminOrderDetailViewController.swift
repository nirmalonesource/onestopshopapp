//
//  AdminOrderDetailViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 12/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import IBAnimatable

class adminorderdetailcell: UITableViewCell {
    @IBOutlet var lblproductname: UILabel!
  @IBOutlet var txtqty: DropDown!
    
    
}

class AdminOrderDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var lblorderno: UILabel!
      @IBOutlet var lbldate: UILabel!
     // @IBOutlet var lblstatus: UILabel!
      @IBOutlet var lblamt: UILabel!
    @IBOutlet var txtstatus: DropDown!
    
    @IBOutlet var tblorderdetail: UITableView!
    @IBOutlet var tblqty: UITableView!
    @IBOutlet var tblorderdetailheight: NSLayoutConstraint!
    @IBOutlet var tblqtyheight: NSLayoutConstraint!
    
    var OrderDetailList = [[String:Any]]()
    var OrderDict = [String:Any]()
    var quntyarr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
     //   let dict:[String:Any] = OrderList[indexPath.row]
        lblorderno.text = OrderDict["OrderNumber"] as? String
        txtstatus.text = OrderDict["Order_Status"] as? String
        lblamt.text = "Rs " + String(OrderDict["Order_Item_Price"] as? String ?? "0")
        lbldate.text = convertDateString(dateString: OrderDict["CreatedOn"] as? String, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yyyy")
        
        txtstatus.optionArray =  ["Pending","cancel","partially_complete","Complete"]
        txtstatus.isSearchEnable = false
        
        if  OrderDict["Order_Status"] as? String == "complete" {
            txtstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
            tblqtyheight.constant = 0
        }
            else if OrderDict["Order_Status"] as? String == "partially_complete"
            {
                txtstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
                viewDidLayoutSubviews()
            }
        else if OrderDict["Order_Status"] as? String == "pending"
        {
            txtstatus.textColor = UIColor.systemOrange
             viewDidLayoutSubviews()
        }
        else
        {
            txtstatus.textColor = UIColor.systemRed
             viewDidLayoutSubviews()
        }
        
        // dropdown delegate
        
        txtstatus.didSelect(completion: { (selected, index, id)  in
         print(selected,index,id)
      //  print("cell:",indexPath.row)
            
   
         if  selected == "Complete" {
            self.txtstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
            self.tblqtyheight.constant = 0
         }
            else if selected == "partially_complete"
                           {
                            self.txtstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
                            self.tblqtyheight.constant =   self.tblorderdetail.contentSize.height
                           }
         else if selected == "Pending"
         {
            self.txtstatus.textColor = UIColor.systemOrange
            self.tblqtyheight.constant =   self.tblorderdetail.contentSize.height
         }
         else
         {
            self.txtstatus.textColor = UIColor.systemRed
            self.tblqtyheight.constant =   self.tblorderdetail.contentSize.height
         }
        
          })
        
        OrderDetailList = OrderDict["Order_details"] as? [[String : Any]] ?? []
    }
    
    override func viewDidLayoutSubviews() {
         
        if  txtstatus.text == "Complete" {
           self.txtstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
           self.tblqtyheight.constant = 0
        }
        else
        {
             tblqtyheight.constant = tblqty.contentSize.height
        }
        
          self.tblorderdetail.layoutIfNeeded()
         self.tblqty.layoutIfNeeded()
          tblorderdetailheight.constant = tblorderdetail.contentSize.height
       
         
      }
    

      //MARK: - Tableview Data Source
                  
                  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                   
                    if tableView == tblorderdetail {
                        return OrderDetailList.count
                    }
                    else if tableView == tblqty {
                        return OrderDetailList.count
                    }
                     return 0
                     
                  }
                  
                  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
                    if tableView == tblorderdetail {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailcell", for: indexPath) as! orderdetailcell
                                    let dict:[String:Any] = OrderDetailList[indexPath.row]
                                    cell.lblproductname.text = dict["ProductName"] as? String
                                    // cell.lblproductdetail.text = String(dict["UOMQty"] as? String ?? "0") + String(dict["UOMDesc"] as? String ?? "0") + "-" + "Rs " + String(dict["TotalPrice"] as? String ?? "0")
                                    
                                    let Product_detail = dict["Product_detail"] as? [[String:Any]]
                                    if Product_detail?.count ?? 0 > 0 {
                                        cell.lblproductdetail.text = "\(dict["UOMQty"] as? String ?? "0") \(dict["UOMDesc"] as? String ?? "0") - Rs \(dict["TotalPrice"] as? String ?? "0")"
                                    }
                                    else
                                    {
                                        cell.lblproductdetail.text = "Rs \(dict["UnitPrice"] as? String ?? "0") * \(dict["Qty"] as? String ?? "0") QTY"
                                    }
                                    
                                        return cell
            }
            else if tableView == tblqty {
        
                let cell = tableView.dequeueReusableCell(withIdentifier: "adminorderdetailcell", for: indexPath) as! adminorderdetailcell
                        var dict:[String:Any] = OrderDetailList[indexPath.row]
                                    cell.lblproductname.text = dict["ProductName"] as? String
                                    // cell.lblproductdetail.text = String(dict["UOMQty"] as? String ?? "0") + String(dict["UOMDesc"] as? String ?? "0") + "-" + "Rs " + String(dict["TotalPrice"] as? String ?? "0")
                                    
                                    let Product_detail = dict["Product_detail"] as? [[String:Any]]
                                        cell.txtqty.isSearchEnable = false
                                    if Product_detail?.count ?? 0 > 0 {
                                        
                                          cell.txtqty.optionArray =  Product_detail!.compactMap {"\( $0["pktsize"] as? String ?? "0") \( $0["uomdesc"] as? String ?? "0")"}
                                        if dict["PendingUOMQty"] as? String ?? "0" == "0.0" {
                                            cell.txtqty.text = ""
                                        }
                                        else
                                        {
                                            cell.txtqty.text = "\(dict["PendingUOMQty"] as? String ?? "0") \(dict["UOMDesc"] as? String ?? "0")"
                                        }
                                        
//                                        var temparr = [[String:Any]]()
//                                        for dic in Product_detail! {
//                                            if (dict["PendingUOMQty"] as? String ?? "0") == (dic["pktsize"] as? String ?? "0") {
//                                                temparr.append(dic)
//                                                break
//                                            }
//                                            else
//                                            {
//                                                 temparr.append(dic)
//                                            }
//                                        }
//                                        cell.txtqty.optionArray =  temparr.compactMap {"\( $0["pktsize"] as? String ?? "0") \( $0["uomdesc"] as? String ?? "0")"}
                                        dict["DQty"] = dict["PendingUOMQty"] as? String ?? "0";
                                        self.OrderDetailList[indexPath.row] = dict
                                    }
                                    else
                                    {
                                        cell.txtqty.text = "\(dict["PendingQty"] as? String ?? "0")"
                                        if dict["PendingQty"] as? String ?? "0" != "0"{
                                            quntyarr = (1...Int("\(dict["PendingQty"] as? String ?? "0")")!).map{"\($0)"}
                                        }
                                        
                                        cell.txtqty.optionArray =  quntyarr
                                        dict["DQty"] = dict["PendingQty"] as? String ?? "0";
                                        self.OrderDetailList[indexPath.row] = dict
                                    }
                        
                        
                        
                        cell.txtqty.didSelect(completion: { (selected, index, id)  in
                              print(index,id)
                        print("cell:",indexPath.row)
                            let Product_detail = dict["Product_detail"] as? [[String:Any]]
                                                               if Product_detail?.count ?? 0 > 0 {
                                                                let arrr = selected.components(separatedBy: " ")
                                                                dict["DQty"] = arrr[0]
                                                                self.OrderDetailList[indexPath.row] = dict
                            }
                            else
                                                               {
                                                                dict["DQty"] = selected
                                                                self.OrderDetailList[indexPath.row] = dict
                            }
                           
                            
                        })
                                    
                                        return cell
                                       }
                   return UITableViewCell()
                     
                  }
             
             
             func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                 
    
             }
     
     // MARK: - Button Action
        
        @IBAction func back_click(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
    
    @IBAction func submit_click(_ sender: AnimatableButton) {
        
        if OrderDetailList.count > 0 {
            UpdateOrderApi()
        }
        else
        {
          showToast(uiview: self, msg:"Please update order")
        }
    }
    
      // MARK: - API
                 
                   func UpdateOrderApi()
                                          {
                                              if !isInternetAvailable(){
                                                  noInternetConnectionAlert(uiview: self)
                                              }
                                              else
                                              {
                                                
                                                var productarr = [[String:Any]]()
                                                var productarr1 = [[String:Any]]()
                                                var json = String()
                                                for dict in OrderDetailList {
                                                    if dict["isPack"] as! String == "1" {
                                                        
                                                        var singleproduct1 = [
                                                         //   "DLPPrice":dict["CategoryID"] ?? ""
                                                        "isPack":dict["isPack"] ?? "",
                                                        "DUOMQty":dict["DUOMQty"] ?? "",
                                                      //  "DLPID":dict["qty"] ?? ""
                                                        ]
                                                        
                                                        let singleproduct2 = [
                                                            "OrderDetailID":dict["OrderDetailID"] ?? "",
                                                            "DQTY":dict["DQty"] ?? "",
                                                            "UnitPrice":dict["UnitPrice"] ?? ""
                                                        ]
                                                        
                                                        singleproduct1.merge(dict: singleproduct2)
                                                        
                                                        productarr.append(singleproduct1)
                                                    }
                                                    else
                                                    {
                                                        let Details = dict["Product_detail"] as? [[String:Any]]
                                                        var uid = String()
                                                         var price = String()
                                                        if Details?.count ?? 0 > 0 {
                                                              
                                                            for i in Details ?? []  {
                                                               let j = i["pktsize"] as! String
                                                               // let arr = j.components(separatedBy: " ")
                                                                
                                                                let k = dict["DQty"] as? String ?? ""
                                                                let arr1 = k.components(separatedBy: " ")
                                                                if j == arr1[0] {
                                                                    uid = (i["uid"] as? String)!
                                                                    price = (i["price"] as? String)!
                                                                }
                                                                
                                                            }
                                                        }
                                                        
                                                        var singleproduct1 = [
                                                                                                                   
                                                        "DLPPrice":price,
                                                        "OrderDetailID":dict["OrderDetailID"] ?? ""
                                                        
                                                    ]
                                                    
                                                    let singleproduct2 = [
                                                        
                                                        "DQTY":"0",
                                                       // "UnitPrice":dict["isAvailable"] ?? "",
                                                        "isPack":dict["isPack"] ?? "",
                                                        "DUOMQty":dict["DQty"] ?? "",
                                                        "DLPID":uid
                                                    ]
                                                        
                                                        singleproduct1.merge(dict: singleproduct2)
                                                        
                                                        productarr.append(singleproduct1)
                                                    }
                                                    
                                                }
                                                
                                                var dic = [String:Any]()
                                                dic["Order_details"] = productarr
                                               
                                                productarr1.insert(dic, at: 0)
                                                
                                                do {

                                                    //Convert to Data
                                                    let jsonData = try JSONSerialization.data(withJSONObject: productarr1, options: JSONSerialization.WritingOptions.prettyPrinted)

                                                    //Convert back to string. Usually only do this for debugging
                                                    if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                                                       print(JSONString)
                                                        json = JSONString
                                                    }

                                                    //In production, you usually want to try and cast as the root data structure. Here we are casting as a dictionary. If the root object is an array cast as [Any].
    //                                                json = try (JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any])!


                                                } catch {
                                                    print(error)
                                                }
                                                
                                                
                                                   let url = ServiceList.SERVICE_URL+ServiceList.UPDATE_ORDER_API
                                                                 
                                                                 let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                "X-ONESHOP-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                ] as [String : Any]
                                                                 
                                                let parameters = [ "Status" : txtstatus.text ?? "",
                                                                                    "OrderID" : OrderDict["OrderID"]!,
                                                                                    "Products" : json
                                                                     ] as [String : Any]
                                
                                                   callApi(url,
                                                                 method: .post,
                                                                 param: parameters ,
                                                                 extraHeader: header,
                                                                withLoader: true)
                                                         { (result) in
                                                              print("PRODUCTRESPONSE:",result)
                                                             
                                                              if result.getBool(key: "status")
                                                              {
                                                                 let data = result["data"] as? [[String : Any]]
                                                                
                                                               
                                                                self.navigationController?.popViewController(animated: true)
                                                                 showToast(uiview: self, msg: result.getString(key: "message"))
//                                                                UserDefaults.standard.removeObject(forKey: "items")
//                                                                self.productlist = [[String:Any]]()
//                                                                GlobalVariables.globalarray = [[String:Any]]()
//                                                                self.tblproductlist.reloadData()
//                                                                self.calcamt()
                                                                
//                                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThnkyouViewController") as! ThnkyouViewController
//                                                                      self.navigationController?.pushViewController(nextViewController, animated: true)
                                                              }
                                                           else
                                                           {
                                                               showToast(uiview: self, msg: result.getString(key: "message"))
                                                           }
                                                  }
                                              }
                                              
                                          }
    
}
