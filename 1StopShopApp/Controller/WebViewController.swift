//
//  WebViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 09/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var webvw: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let url = URL(string: "https://www.facebook.com/")
        webvw.load(URLRequest(url: url!))
    }
    

    // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
