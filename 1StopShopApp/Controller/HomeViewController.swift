//
//  HomeViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 23/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SideMenu
import iOSDropDown
import IBAnimatable

class admincell: UITableViewCell {
   
    @IBOutlet var lblorderno: UILabel!
    @IBOutlet var lblname: UILabel!
    @IBOutlet var lbldate: UILabel!
    @IBOutlet var lblprice: UILabel!
    @IBOutlet var txtstatus: DropDown!
    
    var btnViewMoreAction : (()->())?
    @IBAction func viewmore_click(_ sender: AnimatableButton) {
         btnViewMoreAction?()
    }
}

class homecell: UITableViewCell {
    @IBOutlet var imgphoto: UIImageView!
    @IBOutlet var lbltitle: UILabel!
    
}

class HomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet var btncart: SSBadgeButton!
    @IBOutlet var txtsearch: UITextField!
    @IBOutlet var tbllist: UITableView!
    @IBOutlet var posterheight: NSLayoutConstraint!
    
      var tablearr = [[String:Any]]()
     var OrderList = [[String:Any]]()
    var FilterOrderListArr = [[String:Any]]()
     var SavedOrderListArr = [[String:Any]]()
     var textsearch = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
         txtsearch.delegate = self
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

           //textField code
        
        if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
        {
           textField.resignFirstResponder()  //if desired
            textsearch = textField.text!
            print(textsearch)
            if textsearch.trimmingString() != "" {
             self.CategoryApi()
            }
            else
            {
                textsearch = ""
                self.CategoryApi()
            }
        }
        else
        {
           
        }

           
           return true
       
       }
    
        func textFieldDidEndEditing(_ textField: UITextField) {
        if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
         {
            textField.resignFirstResponder()  //if desired
             textsearch = textField.text!
             print(textsearch)
             if textsearch.trimmingString() != "" {
              self.CategoryApi()
             }
             else
             {
                 textsearch = ""
                 self.CategoryApi()
             }
         }
         else
         {
            
         }
      }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
                {
                   
                }
                else
                {
                   let  array = SavedOrderListArr.filter({
                          // this is where you determine whether to include the specific element, $0
                    ($0["OrderNumber"]! as AnyObject).localizedCaseInsensitiveContains(string)
                          // or whatever search method you're using instead
                      })

                      FilterOrderListArr = array
                      if FilterOrderListArr.count > 0 {
                          OrderList = FilterOrderListArr
                      }
                      else
                      {
                          OrderList = SavedOrderListArr
                      }
                      FilterOrderListArr = [[:]]
                      tbllist.reloadData()
                }
        
        
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
              {
                  CategoryApi()
              }
              else
              {
                  MyOrderApi()
                  btncart.isHidden = true
                posterheight.constant = 0
              }
        
         btncart.badge = String(GlobalVariables.globalarray.count)
    }
    
    override func viewDidLayoutSubviews() {
        tbllist.layoutIfNeeded()
    }
    
    //MARK: - Tableview Data Source
         
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
              if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
              {
                 return tablearr.count
            }
            else
              {
                 return OrderList.count
            }
         }
         
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
             if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
             {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "homecell", for: indexPath) as! homecell
                            tableView.tableFooterView = UIView()
                            cell.lbltitle.text = tablearr[indexPath.row]["Name"] as? String
                            
                            let imgURL = tablearr[indexPath.row]["Image"] as? String ?? ""
                            cell.imgphoto.sd_setImage(with: URL(string:imgURL), placeholderImage: #imageLiteral(resourceName: "Logo"), options:.progressiveLoad, completed: nil)
                //
                             self.view.layoutIfNeeded()
                       //     self.viewDidLayoutSubviews()
                cell.layoutIfNeeded()
                cell.layoutSubviews()
                             return cell
            }
            else
             {
                let cell = tableView.dequeueReusableCell(withIdentifier: "admincell", for: indexPath) as! admincell
                                   let dict:[String:Any] = OrderList[indexPath.row]
                                   cell.lblorderno.text = dict["OrderNumber"] as? String
                                   cell.lblname.text = dict["CustomerName"] as? String
                                   cell.txtstatus.text = dict["Order_Status"] as? String
                                   cell.lblprice.text = "Rs " + String(dict["Order_Item_Price"] as? String ?? "0")
                                   cell.lbldate.text = convertDateString(dateString: dict["CreatedOn"] as? String, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yyyy")
                                   
                                   if  dict["Order_Status"] as? String == "complete"  || dict["Order_Status"] as? String == "partially_complete"{
                                       cell.txtstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
                                   }
                                   else if dict["Order_Status"] as? String == "pending"
                                   {
                                       cell.txtstatus.textColor = UIColor.systemOrange
                                   }
                                   else
                                   {
                                       cell.txtstatus.textColor = UIColor.systemRed
                                   }
                
                cell.txtstatus.optionArray =  ["Pending","cancel","Complete"]
                cell.txtstatus.isSearchEnable = false
                
                cell.txtstatus.didSelect(completion: { (selected, index, id)  in
                    print(selected,index,id)
                   print("cell:",indexPath.row)
                    if  selected == "Complete" {
                        cell.txtstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
                    }
                    else if selected == "Pending"
                    {
                        cell.txtstatus.textColor = UIColor.systemOrange
                    }
                    else
                    {
                        cell.txtstatus.textColor = UIColor.systemRed
                    }
                   
                     })
                
                
                  cell.btnViewMoreAction = { () in
                    
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AdminOrderDetailViewController") as! AdminOrderDetailViewController
                    nextViewController.OrderDict = self.OrderList[indexPath.row]
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                    
                }
                
                                    return cell
            }
            
         }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
               {
                   return 110
               }
               else
               {
                   return 180
               }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
        {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryViewController") as! SubCategoryViewController
                nextViewController.categorydictdata = tablearr[indexPath.row]
                self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            
        }
                
    }
    
    
     // MARK: - API
    
      func CategoryApi()
                             {
                                 if !isInternetAvailable(){
                                     noInternetConnectionAlert(uiview: self)
                                 }
                                 else
                                 {
                                    let url = ServiceList.SERVICE_URL+ServiceList.CATEGORY_API
                                   
                                   let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                              "X-ONESHOP-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                              ] as [String : Any]
                                   
                                   let parameters = [ "search" : textsearch ] as [String : Any]
                   
                                      callApi(url,
                                                    method: .post,
                                                    param: parameters ,
                                                    extraHeader: header,
                                                   withLoader: true)
                                            { (result) in
                                                 print("FAVOURITERESPONSE:",result)
                                                
                                                 if result.getBool(key: "status")
                                                 {
                                                    let data = result["data"] as! [[String : Any]]
                                                    self.tablearr = data
                                                    DispatchQueue.main.async {
                                                        self.tbllist.reloadData()
                                                        self.tbllist.setContentOffset(.zero, animated: true)
                                                    }
                                                    
                                                 }
                                              else
                                              {
                                                  showToast(uiview: self, msg: result.getString(key: "message"))
                                              }
                                     }
                                 }
                                 
                             }
    
    func MyOrderApi()
              {
                  if !isInternetAvailable(){
                      noInternetConnectionAlert(uiview: self)
                  }
                  else
                  {
                     let url = ServiceList.SERVICE_URL+ServiceList.ORDER_LIST_API
                    
                    let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                               "X-ONESHOP-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                               ] as [String : Any]
                    
                   let parameters = [ :
                      ] as [String : Any]
    
                       callApi(url,
                                     method: .post,
                                     param: parameters ,
                                     extraHeader: header,
                                    withLoader: true)
                             { (result) in
                                  print("PRODUCTRESPONSE:",result)
                                 
                                  if result.getBool(key: "status")
                                  {
                                     let data = result["data"] as? [[String : Any]]
                                      self.OrderList = data ?? []
                                     self.SavedOrderListArr = data ?? []
                                     self.tbllist.reloadData()
                                  }
                               else
                               {
                                   showToast(uiview: self, msg: result.getString(key: "message"))
                               }
                      }
                  }
                  
              }
    
    func UpdateOrderApi()
              {
                  if !isInternetAvailable(){
                      noInternetConnectionAlert(uiview: self)
                  }
                  else
                  {
                     let url = ServiceList.SERVICE_URL+ServiceList.UPDATE_ORDER_API
                    
                    let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                               "X-ONESHOP-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                               ] as [String : Any]
                    
                    let parameters = [ "Status" : "",
                                       "OrderID" : "",
                                       "Products" : ""
                        ] as [String : Any]
    
                       callApi(url,
                                     method: .post,
                                     param: parameters ,
                                     extraHeader: header,
                                    withLoader: true)
                             { (result) in
                                  print("PRODUCTRESPONSE:",result)
                                 
                                  if result.getBool(key: "status")
                                  {
                                     let data = result["data"] as? [[String : Any]]
                                      self.OrderList = data ?? []
                                    self.SavedOrderListArr = data ?? []
                                     self.tbllist.reloadData()
                                  }
                               else
                               {
                                   showToast(uiview: self, msg: result.getString(key: "message"))
                               }
                      }
                  }
                  
              }
    
 // MARK: - Button Action
    
    @IBAction func menu_click(_ sender: UIButton) {
        
        let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
              menu.presentationStyle.menuStartAlpha = 1.0
              menu.presentationStyle.presentingEndAlpha = 0.5
              present(menu, animated: true, completion: nil)
    }
    
    @IBAction func cart_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func poster_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
        nextViewController.isfromposter = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    

}
