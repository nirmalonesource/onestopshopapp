//
//  LoginViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 23/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet var lbldescr: UILabel!
    @IBOutlet var txtemail: UITextField!
    @IBOutlet var txtpassword: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func validation()
         {
             self.view.endEditing(true)
             if txtemail.text == "" && txtpassword.text == ""
             {
              showToast(uiview: self, msg: "Please enter all fields")
             }
                 
             else if !txtemail.isValidEmail()
             {
              showToast(uiview: self, msg: "Please enter EmailId")
             }
                 
             else if txtpassword.text == ""
             {
              showToast(uiview: self, msg: "Please enter password")
             }
                 
             else
             {
                 LoginApi()
             }
         }
      
       func LoginApi()
             {
                 if !isInternetAvailable(){
                     noInternetConnectionAlert(uiview: self)
                 }
                 else
                 {
                  let parameters = ["email" : txtemail.text!, "password" : txtpassword.text!,
                                    "device_token" : UIDevice.current.identifierForVendor!.uuidString,"PlayerID" : StoredData.shared.playerId ?? "123"
                         ] as [String : Any]
                    let url = ServiceList.SERVICE_URL+ServiceList.LOGIN_API
                      callApi(url,
                                    method: .post,
                                    param: parameters, withLoader: true)
                            { (result) in
                                 print("LOGINRESPONSE:",result)
                                 if result.getBool(key: "status")
                                 {
                                  
                                  let dict = result.getDictionary(key: "data")
                                   var dictuser = dict.getDictionary(key: "user")
                                  for (key, value) in dictuser {
                                      let val : NSObject = value as! NSObject;
                                      dictuser[key] = val.isEqual(NSNull()) ? "" : value
                                  }
                                //  dictuser["login_token"] = dict.getString(key: "login_token")
                                  print(dictuser)
                                  UserDefaults.standard.setUserDict(value: dictuser)
                                  UserDefaults.standard.setIsLogin(value: true)
                                  UserDefaults.standard.set(dict.getString(key: "login_token"), forKey: "login_token")
                                  
                                  let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                      self.navigationController?.pushViewController(nextViewController, animated: true)
                                 }
                              else
                                 {
                                  showToast(uiview: self, msg: result.getString(key: "message"))
                              }
                     }
                 }
                 
             }

  
    // MARK: - Button Action

    @IBAction func login_click(_ sender: UIButton) {
        
        validation()
    }
    @IBAction func forgotpass_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func account_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
}
