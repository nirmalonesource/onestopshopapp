//
//  SideMenuViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 26/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable

class sidemenutablecell: UITableViewCell {
    @IBOutlet var imgicon: UIImageView!
    @IBOutlet var lbltitle: UILabel!
    
   // @IBOutlet weak var btnarrow: UIImageView!
    
}

class SideMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
   
    @IBOutlet var btnprofile: AnimatableButton!
    @IBOutlet var lblusername: UILabel!
    @IBOutlet var lblemail: UILabel!
    @IBOutlet var tblsidemenu: UITableView!
  
    var menuarr = [String]()
    var menuimgarr = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblusername.text = UserDefaults.standard.getUserDict()["username"] as? String ?? ""
        lblemail.text = UserDefaults.standard.getUserDict()["email"] as? String ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.getUserDict()["role_id"] as? String ?? "" == "3"
        {
            menuarr = ["Home","My Order","Cart","Terms & Conditions","Help","Logout"]
            menuimgarr = ["home","Cart","Cart","terms&condition","help","logout"]
        }
        else
        {
            menuarr = ["Home","Help","Logout"]
            menuimgarr = ["home","help","logout"]
        }
        
        tblsidemenu.reloadData()
    }
    
    
    //MARK: - Tableview Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sidemenutablecell", for: indexPath) as! sidemenutablecell
       tableView.tableFooterView = UIView()
        
         cell.lbltitle.text = menuarr[indexPath.row]
        let imageName = menuimgarr[indexPath.row]
        cell.imgicon.image = UIImage(named: imageName)
        //cell.imgicon.image = UIImage(named: menuimgarr[indexPath.row])
       // cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example

        let currentCell = tableView.cellForRow(at: indexPath!) as! sidemenutablecell
      
                if currentCell.lbltitle.text == "Home"
                {
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                    // self.dismiss(animated: true, completion: nil)
                }
                    
                else  if currentCell.lbltitle.text == "My Order"
                {
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderViewController") as! MyOrderViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                    
                else  if currentCell.lbltitle.text == "Cart"
                {
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                    
                else  if currentCell.lbltitle.text == "Terms & Conditions"
                {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                }

                else if currentCell.lbltitle.text == "Logout"
                {
                let alertController = UIAlertController(title: "", message: "Are you sure you want to Logout?", preferredStyle:UIAlertController.Style.alert)
                        
                if let popoverController = alertController.popoverPresentationController {
                            popoverController.sourceView = self.view //to set the source of your alert
                            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                            }

                        alertController.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default)
                          { action -> Void in
                            // Put your code here
                            self.LogoutApi()
                           
                          })
                        alertController.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.destructive)
                        { action -> Void in
                          // Put your code here
                        })
                        self.present(alertController, animated: true, completion: nil)
                     }
               
        
        tableView.deselectRow(at: indexPath!, animated: true)
    }
    
    //MARK: - Button Action Method
    @IBAction func profile_click(_ sender: AnimatableButton) {
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
//        nextViewController.isedit = true
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
     //MARK: - Web Services
    
    func LogoutApi()
        {
                        if !isInternetAvailable(){
                            noInternetConnectionAlert(uiview: self)
                        }
                        else
                        {
                      let parameters = [
                                        "device_token" : UIDevice.current.identifierForVendor!.uuidString
                                       ] as [String : Any]
                           
                           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                     "X-ONESHOP-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                     ] as [String : Any]
                        
                            let url = ServiceList.SERVICE_URL+ServiceList.LOGOUT_API
                             callApi(url,
                                     method: .post,
                                     param: parameters ,
                                     extraHeader: header,
                                     withLoader: true)
                                   { (result) in
                                        print("LOGOUTRESPONSE:",result)
                                        if result.getBool(key: "status")
                                        {
                                         
                                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                    UserDefaults.standard.setIsLogin(value: false)
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                        }
                                     else
                                        {
                                         
                                            showToast(uiview: self, msg: result.getString(key: "message"))
                                     }
                            }
            }
                        
      }

   
   
    
    
    
}
