//
//  CartViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 30/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import IBAnimatable

class CartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
   // @IBOutlet var lbltitle: UILabel!
    @IBOutlet var tblproductlist: UITableView!
    
    @IBOutlet var lblsubtotal: UILabel!
    @IBOutlet var lblDC: UILabel!
    @IBOutlet var lbltotal: UILabel!
    
    
     var productlist = [[String:Any]]()
    var  subcategorydictdata = [String:Any]()
    var quntyarr = (1...50).map{"\($0)"}
    var total = Double()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // lbltitle.text = subcategorydictdata["SubcategoryName"] as? String
      //  ProductApi()
        tblproductlist.tableFooterView = UIView()
        
       calcamt()
        
        
    }
    func calcamt()  {
        if UserDefaults.standard.object(forKey: "items") != nil {
                   GlobalVariables.globalarray = UserDefaults.standard.array(forKey: "items") as! [[String : Any]]
                   productlist = GlobalVariables.globalarray
                      }
        total = 0
        for dict in productlist {
             let Details = dict["Details"] as? [[String:Any]]
                           
                           if Details?.count ?? 0 > 0 {
                               
                               for i in Details ?? []  {
                                  let j = i["pktsize"] as! String
                                  // let arr = j.components(separatedBy: " ")
                                   
                                   let k = dict["qty"] as! String
                                   let arr1 = k.components(separatedBy: " ")
                                   if j == arr1[0] {
                                    total = Double((i["price"] as! String))! + total
                                   }
                               }
                           }
                           else
                           {
                              total = (Double((dict["Price"] as! String))! * Double((dict["qty"] as! String))!) + total
                           }
        }
        
        lblsubtotal.text = String("RS \(total)")
        lbltotal.text =  String("RS \(total)")
    }
        //MARK: - Tableview Data Source
             
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
                return productlist.count
                
             }
             
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              
                 let cell = tableView.dequeueReusableCell(withIdentifier: "productcell", for: indexPath) as! productcell
                
                cell.lbltitle.text = productlist[indexPath.row]["ProductName"] as? String
                cell.lblprice.text = "RS \(productlist[indexPath.row]["Price"] as? String ?? "")"
                
                let imgURL = productlist[indexPath.row]["ProductImage"] as? String ?? ""
                                     cell.imgphoto.sd_setImage(with: URL(string:imgURL), placeholderImage: #imageLiteral(resourceName: "Logo"), options:.progressiveLoad, completed: nil)
                
                cell.btnaddcart.tag = indexPath.row
                cell.txtqty.tag = indexPath.row
                cell.txtqty.delegate = self
                cell.txtqty.addTarget(self, action: #selector(CartViewController.textFieldDidChange(_:)), for: .editingChanged)

//                cell.btnaddcart.addTarget(self, action:#selector(addcartclick(_:event:)), for: .touchUpInside)
                cell.txtqty.text = productlist[indexPath.row]["qty"] as? String ?? ""
                 cell.btnCartAction = { () in
                    
                    print("QNTY:",cell.txtqty.text ?? "")
                     print("cell:",indexPath.row)
                    
                    self.popupAlert(title: "", message: "Are you sure want to remove this product from cart?", actionTitles: ["NO","YES"], actions:[{action1 in

                    },{action2 in
                         self.productlist.remove(at: indexPath.row)
                                           GlobalVariables.globalarray = self.productlist
                                           UserDefaults.standard.set(GlobalVariables.globalarray, forKey: "items")
                                           UserDefaults.standard.synchronize()
                                           tableView.reloadData()
                        self.calcamt()
                    }, nil])

                   
                }
                
                let Details = productlist[indexPath.row]["Details"] as? [[String:Any]]
                
                if Details?.count ?? 0 > 0 {
                      cell.lbltotalprice.isHidden = false
                  //  let arr = Details!.compactMap {"\( $0["pktsize"] as? String ?? "0") \( $0["uomdesc"] as? String ?? "0")"}
                    cell.txtqty.optionArray =  Details!.compactMap {"\( $0["pktsize"] as? String ?? "0") \( $0["uomdesc"] as? String ?? "0")"}
                     cell.txtqty.isSearchEnable = false
                    
                    for i in Details ?? []  {
                       let j = i["pktsize"] as! String
                       // let arr = j.components(separatedBy: " ")
                        
                        let k = cell.txtqty.text!
                        let arr1 = k.components(separatedBy: " ")
                        if j == arr1[0] {
                            cell.lbltotalprice.text = "RS \(i["price"] as? String ?? "0")"
                        }
                        
                    }
                }
                else
                {
                    cell.lbltotalprice.isHidden = true
                    cell.txtqty.optionArray =  quntyarr
                    cell.txtqty.isSearchEnable = true
                }
                
                
              //  cell.txtqty.optionIds =  quntyarr.compactMap {Int( $0["CategoryID"] as? String ?? "0")}
   cell.txtqty.didSelect(completion: { (selected, index, id)  in
          print(index,id)
    print("cell:",indexPath.row)
    
           var dict:[String:Any] = self.productlist[indexPath.row]
                                    dict["qty"] = selected
   
    if Details?.count ?? 0 > 0
    {
        cell.lbltotalprice.text = "RS \(Details?[index]["price"] as? String ?? "0")"
    }

                                    let index1 = GlobalVariables.globalarray.firstIndex(where: { dictionary in
                                                                         guard let value = dictionary["ProductID"] as? String
                                                                           else { return false }
                                                                         return value == dict["ProductID"] as? String
                                                                       })
    if index1 != nil {
                                                                        GlobalVariables.globalarray.remove(at: index1!)
                                                                        GlobalVariables.globalarray.insert(dict, at: index1!)
                                                                       // GlobalVariables.globalarray.append(dict)
                                                                       }
                                    else
                                                                       {
                                                                //         GlobalVariables.globalarray.append(dict)
                                    }
    UserDefaults.standard.set(GlobalVariables.globalarray, forKey: "items")
    UserDefaults.standard.synchronize()
   // self.tblproductlist.reloadData()
    self.calcamt()
      })
                 return cell
             }
        
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
//                     let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryViewController") as! SubCategoryViewController
//                              nextViewController.dictdata = productlist[indexPath.row]
//                    self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        print("TEXT",textField.text ?? "")
        if textField.text ?? "" != "" {
            var dict:[String:Any] = self.productlist[textField.tag]
            
                dict["qty"] = textField.text
                                               
                                               let index = GlobalVariables.globalarray.firstIndex(where: { dictionary in
                                                                                    guard let value = dictionary["ProductID"] as? String
                                                                                      else { return false }
                                                                                    return value == dict["ProductID"] as? String
                                                                                  })
                                                                                  if let index = index {
                                                                                      GlobalVariables.globalarray.remove(at: index)
                                                                                   GlobalVariables.globalarray.append(dict)
                                                                                  }
                                               else
                                                                                  {
                                                                           //         GlobalVariables.globalarray.append(dict)
                                               }
               UserDefaults.standard.set(GlobalVariables.globalarray, forKey: "items")
               UserDefaults.standard.synchronize()
             self.calcamt()
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
           let compSepByCharInSet = string.components(separatedBy: aSet)
           let numberFiltered = compSepByCharInSet.joined(separator: "")
           return string == numberFiltered
    }
    
      @objc func addcartclick(_ sender: UIButton, event: Any){
        
        
    }

    

   // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cart_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func order_click(_ sender: UIButton) {
        
        if productlist.count > 0 {
            ProductOrderApi()
        }
        else
        {
          showToast(uiview: self, msg:"Please Add Items to cart for order")
        }
    }
    
    // MARK: - API
             
               func ProductOrderApi()
                                      {
                                          if !isInternetAvailable(){
                                              noInternetConnectionAlert(uiview: self)
                                          }
                                          else
                                          {
                                             let url = ServiceList.SERVICE_URL+ServiceList.PLACE_ORDER_API
                                            
                                            var productarr = [[String:Any]]()
                                            var json = String()
                                            for dict in productlist {
                                                if dict["isPack"] as! String == "1" {
                                                    var singleproduct1 = [
                                                        
                                                        "CategoryID":dict["CategoryID"] ?? "",
                                                        "CategoryName":dict["CategoryName"] ?? "",
                                                        "PKTSize":dict["PKTSize"] ?? "",
                                                        "Price":dict["Price"] ?? "",
                                                        "ProductCode":dict["ProductCode"] ?? "",
                                                        "ProductID":dict["ProductID"] ?? "",
                                                        "ProductImage":dict["ProductImage"] ?? "",
                                                        "ProductName":dict["ProductName"] ?? ""
                                                        
                                                    ]
                                                    
                                                    let singleproduct2 = [
                                                        
                                                        "SubcategoryID":dict["SubcategoryID"] ?? "",
                                                        "SubcategoryName":dict["SubcategoryName"] ?? "",
                                                        "UOMDesc":dict["UOMDesc"] ?? "",
                                                        "UOMID":dict["UOMID"] ?? "",
                                                        "fromCart":false,
                                                        "isAvailable":dict["isAvailable"] ?? "",
                                                        "isPack":dict["isPack"] ?? "",
                                                        "qty":dict["qty"] ?? ""
                                                    ]
                                                    
                                                    singleproduct1.merge(dict: singleproduct2)
                                                    
                                                    productarr.append(singleproduct1)
                                                }
                                                else
                                                {
                                                    let Details = dict["Details"] as? [[String:Any]]
                                                    var uid = String()
                                                    if Details?.count ?? 0 > 0 {
                                                          
                                                        for i in Details ?? []  {
                                                           let j = i["pktsize"] as! String
                                                           // let arr = j.components(separatedBy: " ")
                                                            
                                                            let k = dict["qty"] as? String ?? ""
                                                            let arr1 = k.components(separatedBy: " ")
                                                            if j == arr1[0] {
                                                                uid = (i["uid"] as? String)!
                                                            }
                                                            
                                                        }
                                                    }
                                                    
                                                    var singleproduct1 = [
                                                        
                                                        "CategoryID":dict["CategoryID"] ?? "",
                                                        "CategoryName":dict["CategoryName"] ?? "",
                                                        "PKTSize":dict["PKTSize"] ?? "",
                                                        "Price":dict["Price"] ?? "",
                                                        "ProductCode":dict["ProductCode"] ?? "",
                                                        "ProductID":dict["ProductID"] ?? "",
                                                        "ProductImage":dict["ProductImage"] ?? "",
                                                        "ProductName":dict["ProductName"] ?? "",
                                                        "LPPrice":String(total),
                                                        "LPID":uid
                                                        
                                                    ]
                                                    
                                                    

                                                    
                                                    let singleproduct2 = [
                                                        
                                                        "SubcategoryID":dict["SubcategoryID"] ?? "",
                                                        "SubcategoryName":dict["SubcategoryName"] ?? "",
                                                        "UOMDesc":dict["UOMDesc"] ?? "",
                                                        "UOMID":dict["UOMID"] ?? "",
                                                        "fromCart":false,
                                                        "isAvailable":dict["isAvailable"] ?? "",
                                                        "isPack":dict["isPack"] ?? "",
                                                        "qty":dict["qty"] ?? "",
                                                        "UOMQty":dict["qty"] as? String ?? ""
                                                    ]
                                                    
                                                    singleproduct1.merge(dict: singleproduct2)
                                                    
                                                    productarr.append(singleproduct1)
                                                }
                                                
                                            }
                                            
                                            do {

                                                //Convert to Data
                                                let jsonData = try JSONSerialization.data(withJSONObject: productarr, options: JSONSerialization.WritingOptions.prettyPrinted)

                                                //Convert back to string. Usually only do this for debugging
                                                if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                                                   print(JSONString)
                                                    json = JSONString
                                                }

                                                //In production, you usually want to try and cast as the root data structure. Here we are casting as a dictionary. If the root object is an array cast as [Any].
//                                                json = try (JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any])!


                                            } catch {
                                                print(error)
                                            }
                                            
                                            
                                            let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                       "X-ONESHOP-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                       ] as [String : Any]
                                            
                                             let parameters = [ "SubTotal" : String(total) ,
                                                                "DeliveryCharge" : String("0.0") ,
                                                                "TotalPrice" : String(total),
                                                                "Products":json
                                              ] as [String : Any]
                            
                                               callApi(url,
                                                             method: .post,
                                                             param: parameters ,
                                                             extraHeader: header,
                                                            withLoader: true)
                                                     { (result) in
                                                          print("PRODUCTRESPONSE:",result)
                                                         
                                                          if result.getBool(key: "status")
                                                          {
                                                             let data = result["data"] as? [[String : Any]]
                                                          
                                                            UserDefaults.standard.removeObject(forKey: "items")
                                                            self.productlist = [[String:Any]]()
                                                            GlobalVariables.globalarray = [[String:Any]]()
                                                            self.tblproductlist.reloadData()
                                                            self.calcamt()
                                                            
                                                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThnkyouViewController") as! ThnkyouViewController
                                                                  self.navigationController?.pushViewController(nextViewController, animated: true)
                                                          }
                                                       else
                                                       {
                                                           showToast(uiview: self, msg: result.getString(key: "message"))
                                                       }
                                              }
                                          }
                                          
                                      }
         
    
}
