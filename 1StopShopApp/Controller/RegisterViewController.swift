//
//  RegisterViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 23/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet var lbldescr: UILabel!
    @IBOutlet var txtusername: UITextField!
    @IBOutlet var txtemail: UITextField!
    @IBOutlet var txtmono: UITextField!
    @IBOutlet var txtpassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
     func validation()
        {
            self.view.endEditing(true)
            if txtusername.text == ""
            {
             showToast(uiview: self, msg: "Please enter User Name")
            }
                else if !txtemail.isValidEmail()
                {
                 showToast(uiview: self, msg: "Please enter EmailId")
                }
            else if  txtmono.text?.count ?? 0 < 10
            {
            showToast(uiview: self, msg: "Please enter valid Mobile Number")
            }
            
    //        else if txtcountry.text == ""
    //        {
    //         showToast(uiview: self, msg: "Please select Country")
    //        }
    //        else if txtmobileno.text == ""
    //        {
    //        showToast(uiview: self, msg: "Please enter Mobile Number")
    //        }
    //        else if txtgender.text == ""
    //        {
    //        showToast(uiview: self, msg: "Please select Gender")
    //        }
    //        else if txtdob.text == ""
    //        {
    //        showToast(uiview: self, msg: "Please select DOB")
    //        }
//            else if txtselectlocation.text == ""
//            {
//            showToast(uiview: self, msg: "Please select Location")
//            }
                
            else if txtpassword.text == ""
            {
               // isValidated(txtpassword.text!)
            showToast(uiview: self, msg: "Please enter Password")
            }
//            else if txtconfirmpass.text == "" && !isedit
//            {
//            showToast(uiview: self, msg: "Please enter Confirm Password")
//            }
//            else if txtconfirmpass.text != txtpassword.text && !isedit
//            {
//            showToast(uiview: self, msg: "Password and Confirm Confirm Password should be same")
//            }
                
            else
            {
                
                 RegisterApi()
                
            }
        }
    //     func isValidated(_ password: String) -> Bool {
    //        var lowerCaseLetter: Bool = false
    //        var upperCaseLetter: Bool = false
    //       // var digit: Bool = false
    //        var specialCharacter: Bool = false
    //
    //
    //        if password.count  >= 8 {
    //            for char in password.unicodeScalars {
    //                if !lowerCaseLetter {
    //                    lowerCaseLetter = CharacterSet.lowercaseLetters.contains(char)
    //                    showToast(uiview: self, msg: "It should contain atleast one lower case character")
    //
    //                }
    //                if !upperCaseLetter {
    //                    upperCaseLetter = CharacterSet.uppercaseLetters.contains(char)
    //                    showToast(uiview: self, msg: "It should contain atleast one upper case character")
    //                }
    //
    //                if !specialCharacter {
    //                    specialCharacter = CharacterSet.punctuationCharacters.contains(char)
    //                    showToast(uiview: self, msg: "It should contain atleast one special character")
    //                }
    //            }
    //            if specialCharacter || ( lowerCaseLetter && upperCaseLetter) {
    //                //do what u want
    //                return true
    //            }
    //            else {
    //                return false
    //            }
    //        }
    //        return false
    //    }

        //MARK: - Web Services
        
         func RegisterApi()
                   {
                       if !isInternetAvailable(){
                           noInternetConnectionAlert(uiview: self)
                       }
                       else
                       {
                    //    var parameters = [:] as [String : Any]
                      let  parameters = [
                                  "email" : txtemail.text ?? "",
                                  "password" : txtpassword.text ?? "",
                                  "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                                  "mobile_no" : txtmono.text ?? "",
                                  "username" :  txtusername.text ?? "",
                        ] as [String : Any]
                        
                        let url = ServiceList.SERVICE_URL+ServiceList.APP_REGISTER_API
                        
                            callApi(url,
                                    method: .post,
                                    param: parameters as! [String : Any],
                                    withLoader: true)
                                  { (result) in
                                   //    print("LOGINRESPONSE:",result)
                                       if result.getBool(key: "status")
                                       {
                                        
                                        
                            let alertController = UIAlertController(title:"",message:result.getString(key: "message"), preferredStyle:UIAlertController.Style.alert)
                                                      
                                        
                                    if let popoverController = alertController.popoverPresentationController {
                                                                 popoverController.sourceView = self.view //to set the source of your alert
                                                                 popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                                                                           popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                                                                       }

                                                      alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                                                        { action -> Void in
                                                          
                                                          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                                           self.navigationController?.pushViewController(nextViewController, animated: true)

                                                        })
                                                      self.present(alertController, animated: true, completion: nil)
                                        
                                
                                       }
                                    else
                                       {
                                        showToast(uiview: self, msg: result.getString(key: "message"))
                                    }
                           }
                       }
                       
                   }
    
    // MARK: - Button Action

    @IBAction func register_click(_ sender: UIButton) {
        
        validation()
    }
   
    
    @IBAction func account_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
   

}
