//
//  SubCategoryViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 27/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class subcategorycell: UICollectionViewCell {
    
  
    @IBOutlet weak var imgproductname: UIImageView!
    
    
    @IBOutlet weak var lblproductname: UILabel!
    
 
    
    
}

class SubCategoryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var btncart: SSBadgeButton!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var collsubcategory: UICollectionView!
    

      var  Subcategorylist = [[String:Any]]()
     var  categorydictdata = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lbltitle.text = categorydictdata["Name"] as? String
        SubCategoryApi()
    }
    override func viewWillAppear(_ animated: Bool) {
         btncart.badge = String(GlobalVariables.globalarray.count)
    }

     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return Subcategorylist.count
           
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"subcategorycell", for: indexPath) as! subcategorycell
       
               let imgURL = Subcategorylist[indexPath.row]["SubCategoryImage"] as? String ?? ""
               cell.imgproductname.sd_setImage(with: URL(string:imgURL), placeholderImage: #imageLiteral(resourceName: "Logo"),options:.progressiveLoad, completed: nil)
               
           cell.lblproductname.text! = Subcategorylist[indexPath.row]["SubcategoryName"] as? String ?? ""
           
             

            return cell
       }
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
                
         {
                   let padding: CGFloat =  10
                   let collectionViewSize = collectionView.frame.size.width - padding

                   return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
           
               
       }
         
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           
         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
                  nextViewController.subcategorydictdata = Subcategorylist[indexPath.row]
        self.navigationController?.pushViewController(nextViewController, animated: true)
          
       }
    
    
    // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cart_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    
    // MARK: - API
    
      func SubCategoryApi()
                             {
                                 if !isInternetAvailable(){
                                     noInternetConnectionAlert(uiview: self)
                                 }
                                 else
                                 {
                                    let url = ServiceList.SERVICE_URL+ServiceList.SUB_CATEGORY_API
                                   
                                   let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                              "X-ONESHOP-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                              ] as [String : Any]
                                   
                                    let parameters = [ "category_id" : categorydictdata["CategoryID"]! ] as [String : Any]
                   
                                      callApi(url,
                                                    method: .post,
                                                    param: parameters ,
                                                    extraHeader: header,
                                                   withLoader: true)
                                            { (result) in
                                                 print("subcategoryRESPONSE:",result)
                                                
                                                 if result.getBool(key: "status")
                                                 {
                                                    let data = result["data"] as! [[String : Any]]
                                                    self.Subcategorylist = data
                                                    self.collsubcategory.reloadData()
                                                 }
                                              else
                                              {
                                                  showToast(uiview: self, msg: result.getString(key: "message"))
                                              }
                                     }
                                 }
                                 
                             }
}
