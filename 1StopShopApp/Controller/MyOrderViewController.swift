//
//  MyOrderViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 10/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class myordercell: UITableViewCell {
    @IBOutlet var lblorderno: UILabel!
    @IBOutlet var lbldate: UILabel!
    @IBOutlet var lblstatus: UILabel!
    @IBOutlet var lblamt: UILabel!
    
}

class MyOrderViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet var tblorder: UITableView!
    
    var OrderList = [[String:Any]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        MyOrderApi()
    }
    
      //MARK: - Tableview Data Source
                 
                 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                  
                    return OrderList.count
                    
                 }
                 
                 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                  
                     let cell = tableView.dequeueReusableCell(withIdentifier: "myordercell", for: indexPath) as! myordercell
                    let dict:[String:Any] = OrderList[indexPath.row]
                    cell.lblorderno.text = dict["OrderNumber"] as? String
                    cell.lblstatus.text = dict["Order_Status"] as? String
                    cell.lblamt.text = "Rs " + String(dict["Order_Item_Price"] as? String ?? "0")
                    cell.lbldate.text = convertDateString(dateString: dict["CreatedOn"] as? String, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yyyy")
                    
                    if  dict["Order_Status"] as? String == "complete" {
                        cell.lblstatus.textColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
                    }
                    else if dict["Order_Status"] as? String == "pending"
                    {
                        cell.lblstatus.textColor = UIColor.systemOrange
                    }
                    else
                    {
                        cell.lblstatus.textColor = UIColor.systemRed
                    }
                     return cell
                 }
            
            
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                nextViewController.OrderDict = OrderList[indexPath.row]
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
    
    // MARK: - Button Action
       
       @IBAction func back_click(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
   
    
    // MARK: - API
          
            func MyOrderApi()
                                   {
                                       if !isInternetAvailable(){
                                           noInternetConnectionAlert(uiview: self)
                                       }
                                       else
                                       {
                                          let url = ServiceList.SERVICE_URL+ServiceList.ORDER_LIST_API
                                         
                                         let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                    "X-ONESHOP-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                    ] as [String : Any]
                                         
                                        let parameters = [ :
                                           ] as [String : Any]
                         
                                            callApi(url,
                                                          method: .post,
                                                          param: parameters ,
                                                          extraHeader: header,
                                                         withLoader: true)
                                                  { (result) in
                                                       print("PRODUCTRESPONSE:",result)
                                                      
                                                       if result.getBool(key: "status")
                                                       {
                                                          let data = result["data"] as? [[String : Any]]
                                                           self.OrderList = data ?? []
                                                          self.tblorder.reloadData()
                                                       }
                                                    else
                                                    {
                                                        showToast(uiview: self, msg: result.getString(key: "message"))
                                                    }
                                           }
                                       }
                                       
                                   }

}
