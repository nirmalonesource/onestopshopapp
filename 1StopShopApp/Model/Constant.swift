//
//  Constant.swift
//  JetPayApp
//
//  Created by My Mac on 2/14/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct ReversedGeoLocation {
    let name: String            // eg. Apple Inc.
    let streetName: String      // eg. Infinite Loop
   // let streetNumber: String    // eg. 1
    let city: String            // eg. Cupertino
    let state: String           // eg. CA
    let zipCode: String         // eg. 95014
    let country: String         // eg. United States
    let isoCountryCode: String  // eg. US

    var formattedAddress: String {
        return """
        \(name),
        \(streetName),
        \(city), \(state) \(zipCode)
        \(country)
        """
    }

    // Handle optionals as needed
    init(with placemark: CLPlacemark) {
        self.name           = placemark.name ?? ""
        self.streetName     = placemark.thoroughfare ?? ""
       // self.streetNumber   = placemark.subThoroughfare ?? ""
        self.city           = placemark.locality ?? ""
        self.state          = placemark.administrativeArea ?? ""
        self.zipCode        = placemark.postalCode ?? ""
        self.country        = placemark.country ?? ""
        self.isoCountryCode = placemark.isoCountryCode ?? ""
    }
}

struct MyClassConstants
{
    static var dictGlobalData: [String : AnyObject] = [:]
}

struct CustomerConstants
{
    static var user_id = String()
}

//MARK:- Screen Size

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
}

//MARK:- Custom fonts
struct FontNames {
    
//    static let RajdhaniName = "rajdhani"
//    struct Rajdhani {
        static let RajdhaniBold = "rajdhani-bold"
        static let RajdhaniMedium = "rajdhani-medium"
//    }
}
struct GlobalColor {
    static var SelectedColor = UserDefaults.standard.color(forKey: "ColorCode")
}
struct FontColor {
    
    static let dayfont = 0x000000
    static let nightfont = 0xffffff
    
   
}

struct GlobalVariables {
    static var globalarray = [[String:Any]]()
}

//MARK:- Webservices

 struct ServiceList
 {
  
    
    static let USERNAME = "admin"
    static let PASSWORD = "API@ONESHOP@123"
    
    static var SERVICE_URL = "http://clientsdemoarea.com/projects/oneshop/api/v1/"
  //  static var SocketUrl = UserDefaults.standard.string(forKey: "ChatURL")
   // static var DomainUrl = UserDefaults.standard.string(forKey: "DomainName")
    
    static let TERMS_CONDITION_API = "terms-conditions?mobile=1"
    
    static let SERVICE_AUTH = "auth/"

    static let X_SIMPLE_API_KEY = "ow400c888s4c8wck8w0c0w8co0kc00o0wgoosw80"
    static let AUTHORIZATION_AUTH = "YWRtaW46YWRtaW4="

    static let LOGIN_API = SERVICE_AUTH + "login"
    
    static let APP_REGISTER_API = SERVICE_AUTH + "register"
    
    static let FORGOT_PASSWORD_API = SERVICE_AUTH + "forgot_password"
    
    static let CHANGE_PASSWORD_API = SERVICE_AUTH + "change_password"
    
    static let LOGOUT_API = SERVICE_AUTH + "logout"
    
    
    static let CATEGORY_API = "Category"
    
    static let SUB_CATEGORY_API = "sub-category"
    
    static let PRODUCT_API = "product"
   
    static let UOM_API = "uom"
    
    static let PLACE_ORDER_API = "Order"
    
    static let ORDER_LIST_API = "order/list"
    
    static let UPDATE_ORDER_API = "order/update_order"
    
    static let OFFER_PRODUCT_API = "Product/offer_product"
    
 }

extension UserDefaults{
    
    func setIsLogin(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func getIsLogin()-> Bool {
        return bool(forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func setUserDict(value: [String : Any]){
        set(value, forKey: UserDefaultsKeys.userData.rawValue)
    }
    
    func getUserDict() -> [String : Any]{
        return dictionary(forKey: UserDefaultsKeys.userData.rawValue) ?? [:]
    }
    
}

enum UserDefaultsKeys : String {
    case userData
    case isUserLogin
}

class StoredData: NSObject {
    
    static let shared = StoredData()
    var playerId: String!
   
}
